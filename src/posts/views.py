from . import models
from django.db.models import Count
from django.views.generic import TemplateView, ListView, DetailView
from datetime import datetime, timedelta
from django.shortcuts import get_object_or_404, get_list_or_404

import logging

log = logging.getLogger(__name__)


class IndexView(ListView):
    model = models.Post
    template_name = 'posts/post_list.html'
    paginate_by = 5
    context_object_name = 'posts'


class CategoryView(ListView):
    model = models.Category
    template_name = 'posts/post_list.html'
    paginate_by = 5
    context_object_name = 'posts'

    def get_queryset(self):
        category = get_object_or_404(models.Category, slug=self.kwargs.get('category'))
        return models.Post.objects.filter(category__slug=category.slug)


class PostView(TemplateView):
    template_name = 'posts/post.html'

    def get_context_data(self, **kwargs):
        context = super(PostView, self).get_context_data(**kwargs)
        instance = get_object_or_404(models.Post, slug=kwargs.get('post'))
        instance.visited += 1
        instance.save()
        tags = models.Tag.objects.filter(posts__id=instance.id)
        context.update({
            'post': instance,
            'tags': tags
        })

        return context


class TagView(ListView):
    model = models.Tag
    template_name = 'posts/post_list.html'
    paginate_by = 5
    context_object_name = 'posts'

    def get_queryset(self):
        return models.Post.objects.filter(tag__slug=self.kwargs.get('tag'))
