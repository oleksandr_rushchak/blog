from datetime import datetime, timedelta
from django.db.models import Count

from .models import Category, Post, Tag


def general_content(request):
    return {
        'categories': Category.objects.all(),
        'popular_tags':
            Tag.objects.annotate(used=Count('posts')).order_by('-used')[:15],
        'popular_posts':
            Post.objects.filter(
                created__gte=datetime.now() - timedelta(days=7)
            ).order_by('-visited')[:10],
    }
